<h1>Komputer store</h1>
<h2>Information</h2>
<h4>My first assignment to simulate a computer store with elements such as </h4>
<ul>List of features
<li>PC store where u can browse and buy pc:s</li>
<li>Dynamic changes of features,specs, image and price depending on which pc is selected</li>
<li>A place where you can work and earn salary</li>
<li>A bank where you can get a loan</li>
</ul>

All the different places have some logical restraints on how things work and i will list some of the most important below.

 <dl>
         <dt><b>Banking</b></dt>
         <dd>You cannot take a loan bigger than two times your salary </dd>
         <dd>You cannot take a negative loan or a loan that isnt values </dd>
         <dd>You cannot have more than one loan</dd>
         <dt><b>Working</b></dt>
         <dd>If you have a loan, 90% of salary goes towards balance and the remaining 10% goes and decreases the loan balance(from my understanding of the assignment)</dd>
         <dd>Once you have a loan you will receive another button to repay your loan in full credit</dd>
         <dd>If you pay too much on the loan, you will get the overflow back to your "work-account"</dd>
         <dt><b>Buying laptop</b></dt>
         <dd>You can only buy a pc that you can afford</dd>
</dl>

## Usage

```
Right-click index.html and start with live server and use the site that is created
```

## Maintainers

 * Joachim Nilsson - https://gitlab.com/Klutcharn
