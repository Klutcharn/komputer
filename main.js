//initializing variables
let totalWorked = 0;
let totalBanked = 0;
let LoanValue = 0;
let afterLoanValue = 0;
let haveLoan = 0;
let computers = [];
let boughtPcPrice = 0;
let boughtPcName = '';
let fixImgName = '';
let specsArray = [];

//setting html element to const by id
const computerElement = document.getElementById('pc_selection');
const bankButton = document.getElementById('bank_button');
const workButton = document.getElementById('work_button');
const loanButton = document.getElementById('loan_button');
const repayButton = document.getElementById('repay_button');
const buyButton = document.getElementById('buy_button');
const pcName = document.getElementById('pc_name');
const pcText = document.getElementById('pc_feature');
const pcPrice = document.getElementById('pc_price');
const pcSpecs = document.getElementById('pc_specs');
const pcPic = document.getElementById('pc_pic');

//fetching data from API
fetch('https://noroff-komputer-store-api.herokuapp.com/computers')
   .then((response) => response.json())
   .then((data) => (computers = data))
   .then((computers) => addComputers(computers))
   .catch(function (error) {
      console.log(error);
   });

//Getting each computer from API, setting a default value to html elements
const addComputers = (computers) => {
   computers.forEach((x) => addComputer(x));
   pcName.innerText = computers[0].title;
   pcText.innerText = computers[0].description;
   pcPrice.innerText = Intl.NumberFormat('sv-SE', {
      style: 'currency',
      currency: 'SEK',
   }).format(computers[0].price);
   boughtPcPrice = computers[0].price;
   boughtPcName = computers[0].title;
   pcPic.src =
      'https://noroff-komputer-store-api.herokuapp.com/' + computers[0].image;
   specsArray = computers[0].specs;
   specsArray.forEach((item) => {
      let li = document.createElement('li');
      li.innerText = item;
      pcSpecs.appendChild(li);
   });
};
//making computers show in dropdown-list from API
const addComputer = (computer) => {
   const pcElement = document.createElement('option');
   pcElement.value = computer.id;
   pcElement.appendChild(document.createTextNode(computer.title));
   computerElement.appendChild(pcElement);
};

//changing information about computer dynamically based on which pc is selected in dropdown-list
const HandlePcInfoChange = (e) => {
   const selectedPc = computers[e.target.selectedIndex];
   pcName.innerText = selectedPc.title;
   pcText.innerText = selectedPc.description;
   boughtPcName = selectedPc.title;
   pcPrice.innerText = Intl.NumberFormat('sv-SE', {
      style: 'currency',
      currency: 'SEK',
   }).format(selectedPc.price);
   boughtPcPrice = selectedPc.price;
   pcSpecs.innerText = 'Features';
   specsArray = selectedPc.specs;
   specsArray.forEach((item) => {
      let li = document.createElement('li');
      li.innerText = item;
      pcSpecs.appendChild(li);
      if (selectedPc.image === 'assets/images/5.jpg') {
         fixImgName = selectedPc.image;
         fixImgName = fixImgName.replace('jpg', 'png');
         pcPic.src =
            'https://noroff-komputer-store-api.herokuapp.com/' + fixImgName;
      } else {
         pcPic.src =
            'https://noroff-komputer-store-api.herokuapp.com/' +
            selectedPc.image;
      }
   });
};

//eventlisteners to for changing information based on computer selection
computerElement.addEventListener('change', HandlePcInfoChange);
//eventlisteners for handling button clicks on website.
bankButton.addEventListener('click', banking);
workButton.addEventListener('click', working);
loanButton.addEventListener('click', getLoan);
repayButton.addEventListener('click', rePay);
buyButton.addEventListener('click', buyPc);

// Adding 100 sek to balance based on each click
function working() {
   totalWorked += 100;
   document.getElementById('workMoney').innerText = Intl.NumberFormat('sv-SE', {
      style: 'currency',
      currency: 'SEK',
   }).format(totalWorked);
}

//handling the banking from work balance to banked balance
function banking() {
   //checking if there is a loan
   if (haveLoan === 0) {
      totalBanked += totalWorked;
      totalWorked = 0;
      document.getElementById('money').innerText = Intl.NumberFormat('sv-SE', {
         style: 'currency',
         currency: 'SEK',
      }).format(totalBanked);
      document.getElementById('workMoney').innerText = Intl.NumberFormat(
         'sv-SE',
         { style: 'currency', currency: 'SEK' }
      ).format(totalWorked);
   } else {
      //if there is a loan there are contraints on banked money which is handled below
      totalBanked += totalWorked * 0.9;
      LoanValue = LoanValue - totalWorked * 0.1;
      document.getElementById('money').innerText = Intl.NumberFormat('sv-SE', {
         style: 'currency',
         currency: 'SEK',
      }).format(totalBanked);
      if (LoanValue <= 0) {
         //handling issues with negative loan values and putting overflow back to work balance
         totalWorked = Math.abs(LoanValue);
         document.getElementById('workMoney').innerText = Intl.NumberFormat(
            'sv-SE',
            { style: 'currency', currency: 'SEK' }
         ).format(totalWorked);
         haveLoan = 0;
         document.getElementById('outstanding_loan').innerText =
            Intl.NumberFormat('sv-SE', {
               style: 'currency',
               currency: 'SEK',
            }).format(0);
         //taking away visibility of repay balance and repay button.
         document.getElementById('outstanding_loan').style.display = 'none';
         document.getElementById('outstanding').style.display = 'none';
         document.getElementById('repay_button').style.display = 'none';
      } else {
         totalWorked = 0;
         document.getElementById('workMoney').innerText = Intl.NumberFormat(
            'sv-SE',
            { style: 'currency', currency: 'SEK' }
         ).format(totalWorked);
         document.getElementById('outstanding_loan').innerText =
            Intl.NumberFormat('sv-SE', {
               style: 'currency',
               currency: 'SEK',
            }).format(LoanValue);
      }
   }
}
//handling the banking situation
function getLoan() {
   //checking if you already have a loan or not
   if (haveLoan === 0) {
      LoanValue = Number(prompt('How much money do you want to loan?'));
      //if your loan is above 0 but under two times ur current amount you are allowed a loan.
      if (LoanValue <= 2 * totalBanked && LoanValue > 0) {
         //setting loan status to 1 instead of 0.
         haveLoan = 1;
         totalBanked = totalBanked + LoanValue;
         document.getElementById('money').innerText = Intl.NumberFormat(
            'sv-SE',
            {
               style: 'currency',
               currency: 'SEK',
            }
         ).format(totalBanked);
         document.getElementById('outstanding').style.display = 'block';
         document.getElementById('outstanding_loan').style.display = 'block';
         document.getElementById('outstanding_loan').innerText =
            Intl.NumberFormat('sv-SE', {
               style: 'currency',
               currency: 'SEK',
            }).format(LoanValue);
         document.getElementById('repay_button').style.display = 'block';
      } else if (LoanValue > 2 * totalBanked) {
         //if you are trying to loan too much money you get an alert
         alert(`You have too little money, maximum loan of ${2 * totalBanked}`);
         document.getElementById('money').innerText = Intl.NumberFormat(
            'sv-SE',
            {
               style: 'currency',
               currency: 'SEK',
            }
         ).format(totalBanked);
      } else {
         // if you input anything other than positive numbers u get an alert
         alert('Please enter a positive value above 0');
      }
   } else {
      // if you already have a loan you get an alert.
      alert('You already have a loan!');
   }
}
//handling the repay of loan
function rePay() {
   //allows full credit or work salary to be put towards paying of the loan
   LoanValue = LoanValue - totalWorked;
   if (LoanValue <= 0) {
      //handling situation where balance would be negative on loanValue and putting overflow back to work balance.
      haveLoan = 0;
      document.getElementById('outstanding_loan').innerText = Intl.NumberFormat(
         'sv-SE',
         {
            style: 'currency',
            currency: 'SEK',
         }
      ).format(0);
      totalWorked = Math.abs(LoanValue);
      document.getElementById('workMoney').innerText = Intl.NumberFormat(
         'sv-SE',
         { style: 'currency', currency: 'SEK' }
      ).format(totalWorked);
      // taking away visibility of repay balance and repay button.
      document.getElementById('outstanding_loan').style.display = 'none';
      document.getElementById('outstanding').style.display = 'none';
      document.getElementById('repay_button').style.display = 'none';
   } else {
      //handling the loan value if a positive value after paying of debt
      document.getElementById('outstanding_loan').innerText = Intl.NumberFormat(
         'sv-SE',
         {
            style: 'currency',
            currency: 'SEK',
         }
      ).format(LoanValue);
      totalWorked = 0;
      document.getElementById('workMoney').innerText = Intl.NumberFormat(
         'sv-SE',
         { style: 'currency', currency: 'SEK' }
      ).format(0);
   }
}
//handling the action of purchasing a pc
function buyPc() {
   //initializing a variable that is used to check if you have had enough money to buy selected pc
   let sumLeft = totalBanked - boughtPcPrice;
   //checking if sumLeft is above 0 and reducing balance with amount left and putting alert for the bought PC.
   if (sumLeft >= 0) {
      totalBanked = sumLeft;
      document.getElementById('money').innerText = Intl.NumberFormat('sv-SE', {
         style: 'currency',
         currency: 'SEK',
      }).format(sumLeft);
      alert(`CONGRATULATIONS ON YOUR NEW: ${boughtPcName}`);
   } else {
      // if sumLeft is negative, you instead get alert with how much more money you need before you can buy selected PC.
      alert(
         `Too little money, earn at least: ${Math.abs(
            sumLeft
         )} more before you buy this PC `
      );
   }
}
